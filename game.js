function gameJS(canvas){
	var self = this; // fix context

	if(!canvas || !canvas['getContext']) throw new Error('Arguments');

// default setting //
	this.setting = {
		tile_size: 20, 			// размер клетки
		view_size: 7,			// радиус видимости 
		map_size: 4, 			// размер генерируемой карты
		
		view_borderColor: '#111',	// цвет бордюра		

		block_size: 16, 		// размер блока
		block_color: '#000', 	// цвет
		block_offset: 1, 		// смешение блока относительно поля
		block_type: 'down', 	// left || down || none

		text_color: '#fff', 	// цвет текста
		text_offsetX: 3, 		// смешение текста по оси Х
		text_offsetY: 0, 		// смешение текста по оси У
		text_font: 'bold 12px sans-serif', // шрифт
		text_lineOffset: 78, 	// растояние между элементами
		text_align: 'start', 	// выравнивание
		text_baseline: 'top', 	// выравнивание по высоте 

		border_size: 1, 		// размер бордюра
		border_color: '#000' 	// цвет бордюра
	};

	this.player = {
		x: 0, y: 0,
		view_radius: 3,
		color: "#44f"
	};

// end //

// init //
	this.isStart = false;

	this.canvas = canvas;
	this.context = canvas.getContext("2d");

	// инициализированные объекты карты
	// {id: nameModule}
	this.loadMapObj = {}; 

	// текст 
	this.textObj = {}; // текст
	this.mapObj = {
		wall: 	{id: 0, wall: true, color: '#444'},
		empty: 	{id: 1, color: '#ddd'}, 
		next: 	{id: 2, color: '#000', randomPos: 1, 
			event_type: 'clkEnter',
			script: function(){
				self.map.max++;
				if(self.map.max>self.map.min+3) self.map.min++;
				var size = (Math.round(Math.random() * (self.map.max - self.map.min)) + self.map.min)
				self.map.size = size;
				self.map_gen();

				self.pos_generate();
				//self.draw();
			}
		}
	};

	this.map = {min: this.setting.map_size, 
				max: this.setting.map_size, 
				size: this.setting.map_size};
	this.temp = {};
	this.save = {};
// end //

// keyEvent //
	this.eKey = {};
	this.eKey.list = {}; // {key: [func, func]}

	this.eKey.init = function(elem) {
		elem.onkeydown = function(e){
			self.eKey.call(e.keyCode, 'keydown')
		}

		elem.onkeyup = function(e){
			self.eKey.call(e.keyCode, 'keyup')
		}
	};

	this.eKey.reKey = function(key){
		var r = null;
		if(typeof(key) == 'number' && this.keyCode[key]) r = key;
		if(typeof(key) == 'string') {
			for (e in this.keyCode){
				if(this.keyCode[e] == key) 
					r = e;
			}
		}
		return r;
	};

	this.eKey.fKey = function(key){
		var r = null;
		if(typeof(key) == 'number') r = this.keyCode[key];
		if(typeof(key) == 'string') {
			if(this.keyCode[key] == key) r = key;
		}
		return r;
	}

	this.eKey.add = function(key, func) {
		if(typeof(key) != 'number') key = this.reKey(key);
		if(!key || !func) return;
		if(typeof(this.list[key]) != 'object') this.list[key] = [];

		this.list[key].push(func);
	};

	this.eKey.delete = function(key, e) {
		if(typeof(key) != 'number') key = this.reKey(key);
		if(!key) return;
		if(!e) this.list[key] = [];
		if(typeof(e) == 'number') this.list[key].pop(e);
		if(typeof(e) == 'function') {
			for(el in this.list[key]){
				if(this.list[key][el] == e)
					this.list[key].pop(el); 
			}
		}
	}

	this.eKey.clear = function() {
		this.list = {};
	}

	this.eKey.call = function(key, type) {
		key = this.reKey(key)
		if(!key) return;
		if(type == 'keydown') {
			if(this.keyPress[this.keyCode[key]]) return;
			this.keyPress[this.keyCode[key]] = true;
		}

		if(type == 'keyup') 
			this.keyPress[this.keyCode[key]] = false;
	
		for(el in this.list[key]){
				this.list[key][el].apply(self, [key, type]);
			}
	}

	this.eKey.addKey = function(num, name) {
		this.keyCode[num] = name;
	}

	this.eKey.keyPress = {
		down: false,
		left: false,
		right: false,
		up: false,
		enter: false,
		esc: false
	};

	this.eKey.keyCode = {
		37: "left",
		38: "up",
		39: "right",
		40: "down",
		13: "enter",
		27: "esc"
	};
// end //

// libfunc //
	this.clone = function(obj){
		if(obj == null || typeof(obj) != 'object' ) return obj;

		var tmp = obj.constructor()
		for(var key in obj){
			if(obj.hasOwnProperty(key)){
				tmp[key] = this.clone(obj[key])
			}
		}
		return tmp;
	};
// end //

// modules list//
	this.modules = {};
	this.modules.list = {
		egg: {
			init: function(){
				self.eKey.addKey(67, 'c');
				self.eKey.add('c', function(k, t){
					if(t!='keydown') return;
					var z = function(){
						return Math.round(Math.random() * 255).toString(16);
					};
					self.player.color = '#' + z() + z() + z();
				});

				self.eKey.addKey(82, 'r');
				self.eKey.add('r', function(k, t){
					if(t!='keydown') return;
					var rPos = self.pos_random();
					self.player.x = rPos.x;
					self.player.y = rPos.y;
				})
			}
		},
		size: {
			textObj: { text: "Size #", value: this.map.size},
			events: {
				next: function(){
					this.get_textObj('size').value = this.map.size;
				}
			}
		},

		level: {
			textObj: { text: "Level #", value: 1},
			events: {
				next: function(){
					this.get_textObj('level').value++;
				}
			}
		},

		score: {
			mapObj: {
				id: 15, color: '#fc0', bonus: true, randomPos: 1, 
				script: function(){
					this.get_textObj('score').value += 5;
					this.pos_clear();
				}
			},

			textObj: {text: 'Score #', value: 0},

			events: {
				next: function(){
					this.get_textObj('score').value++;
				}
			}

		},

		radius: {
			mapObj:	{id: 10, color: '#0f0', bonus: true, randomPos: 7, 
				script: function(obj){
					this.player.view_radius++;
					this.pos_clear();

					if(self.player.view_radius == this.setting.view_size){
						obj.randomPos = 0;
					}
				}
			}
		},

		timer: {
			timer: null,

			init: function(obj){
				if(obj.timer) return;
				obj.timer = setInterval((function(){
					if(self.isStart)
					if(self.get_textObj('timer').value > 0){
						self.get_textObj('timer').value--;
					} else {
						alert('You lose. Score '+self.get_textObj('score').value + '\nReload page - new self' );
						self.isStart = false;
					}
				}), 1000)
			},

			disabled: function(obj){
				clearInterval(obj.timer)
			},

			events: {
				next: function(){
					this.get_textObj('timer').value+=10;
				}
			},

			mapObj: {
				id: 10, color: '#55f', bonus: true, randomPos: 7, 
				script: function(){
					self.get_textObj('timer').value += 100;
					self.pos_clear()
				}
			},
			textObj: {text: 'Timer #', value: 100}
		}
	};
// end // 

// module func //
	this.modules.init = function(){
		var list = this.list;

		for (key in list){
			this.enable(key);
		}		
	};

	this.modules.add = function(obj){
		var list = this.list;

		for (key in obj){
			if(!list[name]) list[key] = this.clone(obj[key]);
		}
	};

	this.modules.delete = function(name){
		var list = this.list;

		if(!list[name]) return;
		this.disable(name);
		delete list[name];
	};

	this.modules.reset = function(){
		var list = this.list;

		for (key in list){
			//this.disable(key);
			this.enable(key);
		}
	}

	this.modules.disable = function(name){
		this.toggle(name, false);
	};

	this.modules.enable = function(name){
		this.toggle(name, true);
	};

	this.modules.toggle = function(name, s){
		var obj = this.list[name];
		if(!obj) return;

		if(obj['disabled'] && !s) obj.disabled.call(self, obj);
		if(obj['init'] && s) obj.init.call(self, obj);

		if(obj['mapObj']) 
			if(s) self.mapObj[name] = self.clone(obj.mapObj);
			else delete self.mapObj[name];

		if(obj['textObj']) 
			if(s) self.textObj[name] = self.clone(obj.textObj);
			else delete self.textObj[name];

		if(obj['events']) {
			var e = obj['events']; 
			for(key in e){
				if(s) self.events.add(key, e[key]);
				else self.events.delete(key, e[key]);
			}
		}

		obj.disabled = !s;

	}
// end //

// event //
	this.events = {};
	this.events.list = {};

	this.events.add = function(type, callback){
		var list = this.list;

		if(!list[type]) list[type] = [];
		list[type].push(callback);
	};

	this.events.delete = function(type, callback){
		var list = this.list;

		if(list[type]){
			list[type].forEach(function(e, n){
				if(e == callback) list[type].slice(n, 1);
			})
		}
	};

	this.events.call = function(type){
		var list = this.list;

		if(list[type]){
			list[type].forEach(function(e){
				e.call(self);
			})
		}			
	}

	this.events.clear = function(type){
		if(type) this.list[type] = [];
		else this.list = {};
	}
// end //

// pos func //
	this.pos_random = function(){
		var x = Math.round(Math.random() * (this.map.size-1)) * 2 + 1,
			y = Math.round(Math.random() * (this.map.size-1)) * 2 + 1

		return {x: x, y: y};
	};

	this.pos_generate = function(){
		var rPos = this.pos_random();
		this.player.x = rPos.x;
		this.player.y = rPos.y;

		var mapObj = this.mapObj;
		for(var key in mapObj){
			if(mapObj[key].randomPos) {
				var ver = mapObj[key].randomPos;
				if(ver == 1 || Math.round(Math.random() * (ver - 1)) == 1 ) {
					var rPos = this.pos_random();
					var x = rPos.x;
					var y = rPos.y;

					while((this.map.data[y][x] != 'empty') || 
						((this.player.x == x) && (this.player.y == y))) { 
							rPos = this.pos_random();
							var x = rPos.x;
							var y = rPos.y;
						};

					this.map.data[y][x] = key;
				}
			}
		};
	};

	this.pos_clear = function(x,y){
		x = x || this.player.x;
		y = y || this.player.y;
		
		if(this.map.data[y][x] != 'wall')
			this.map.data[y][x] = 'empty';
	}
// end //

// get func //
	this.get_tile = function (x, y) {
		var out = (this.map.data[y] && this.map.data[y][x]) ? this.map.data[y][x] : false;
		if(out) out = this.mapObj[this.map.data[y][x]]
		return out;
	};

	this.get_nameTile = function (x, y) {
		var out = (this.map.data[y] && this.map.data[y][x]) ? this.map.data[y][x] : false;
		if(out) out = this.map.data[y][x];
		return out;
	};

	this.get_mapObj = function(name){
		return  this.mapObj[name];
	};

	this.get_textObj = function(name){
		return this.textObj[name];
	}

	this.get_size = function(type){
		var out = {}
		var size = this.setting.tile_size * 
				  (this.setting.view_size * 2 + 1) + 
				  this.setting.border_size * 2;

		if (type == 1){
			out.x = out.y = size;
		} else {
			var size_block = this.setting.block_size +
							 this.setting.block_offset;
			var type = this.setting.block_type;

			out.x = size + (type == 'left' ? size_block : 0)
			out.y = size + (type == 'down' ? size_block : 0)
		}
		return out;
	}
// end //

// map //
	this.map_gen = function(size){
		size = size || this.map.size;
		log=function(arr) {
			out = '';
			for(y = 0; y < arr.length; y++){
				for (x = 0; x < arr[y].length; x++){
					out += (arr[y][x] || '0') + ' ';
				}
				out+='\n';
			}
			console.log(out);
		}
		// Объявим массивы для хранения значения множества текущей ячейки, для значения стенки справа и для значения стенки снизу
		var arr = [];
		y_size = x_size = size;
		for(y = 0; y < y_size*2+1; y++){
			arr[y] = [];
			for (x = 0; x < x_size*2+1; x++){
				arr[y][x] = 0;
				if(y==0 || !(x%2) || (x%2 && !(y%2)) || 
					y==y_size*2 || x==x_size*2)
					arr[y][x] = 1;
			}
		}

		var mn = Array(x_size),
			b = Array(x_size),
			k = Array(x_size), 
			q = 1;


		// Цикл по строкам
		for (y = 0; y < y_size; y++) {
			// Проверка принадлежности ячейки в строке к какому-либо множеству     
			for (x = 0; x < x_size; x++){
				if(0 == y) mn[x] = 0;
				k[x] = 0;
				if(1 == b[x]) b[x] = mn[x] = 0;
				if(0 == mn[x]) mn[x] = q++;
			}

			// Создание случайным образом стенок справа и снизу
			for (x = 0; x < x_size; x++) {
				k[x] = Math.floor(2 * Math.random()); 
				b[x] = Math.floor(2 * Math.random());

				if ((0 == k[x] || y == y_size - 1) //право
				&& x != x_size - 1 
				&& mn[x + 1] != mn[x]) {
					var l = mn[x + 1];
					for (j = 0; j < x_size; j++) 
						if(mn[j] == l) mn[j] = mn[x];
					arr[y*2+1][x*2+2]=0;
				}
				if(y != y_size - 1 && 0 == b[x]) {// низ
					arr[y*2+2][x*2+1]=0;
				}
			}

			 // Проверка на замкнутые области.
			for (x = 0; x < x_size; x++) {
				var p = 0,
					l = 0;
				for (j = 0; j < x_size; j++) 
					if(mn[x] == mn[j] && 0 == b[j]) p++; 
					else l++;
				if(0 == p){
					b[x] = 0;
					arr[y*2+2][x*2+1]=0;
				}
			}
		}
		for (y = 0; y < y_size*2+1; y++){
			for (x = 0; x < x_size*2+1; x++){
				arr[y][x] = (arr[y][x] == 1 ? 'wall' : 'empty');
			}
		}
		self.map.data = arr;
	};
// end //

// draw //
	this.draw_tile = function (x, y, colour) {
		x += this.setting.border_size;
		y += this.setting.border_size;

		this.context.fillStyle = colour;
		this.context.fillRect(x, y, this.setting.tile_size, this.setting.tile_size);
	};

	this.draw_player = function(){
		var x = y = this.setting.view_size * this.setting.tile_size + 
					this.setting.tile_size / 2 + this.setting.border_size;

		this.context.fillStyle = this.player.color;
		this.context.beginPath();
		this.context.arc(
			x,
			y,
			this.setting.tile_size / 2 - 1,
			0,
			Math.PI * 2
		);
		
		this.context.fill();
	};

	this.draw_border = function(){
		var size = this.get_size(1);
		var x = size.x,
			y = size.y;

		var x1 = x,
			y1 = y;

		if(this.setting.block_size){
			if (this.setting.block_type == 'left'){
				x += (this.setting.block_size + this.setting.block_offset);
			}

			if (this.setting.block_type == 'down'){
				y += (this.setting.block_size + this.setting.block_offset);
			}
		}

		this.context.fillStyle = this.setting.border_color;
		this.context.fillRect(0, 0, x, y);
	};

	this.draw_block = function(){
		if (!this.setting.block_size || this.setting.block_type == 'none') return;

		var size = this.get_size(1);

		var x = y = this.setting.border_size;

		var	n_x = size.x - x*2, 
			n_y = size.y - y*2; 

		if (this.setting.block_type == 'left'){
			n_x = this.setting.block_size //- setting.block_offset;
			x = size.x + this.setting.block_offset - x;
		}

		if (this.setting.block_type == 'down'){
			n_y = this.setting.block_size //- setting.block_offset;
			y = size.y + this.setting.block_offset - y;
		}

		this.context.fillStyle = this.setting.block_color;
		this.context.fillRect(x, y, n_x, n_y);

		this.draw_text();
	};

	this.draw_text = function(){
		var size = this.get_size(1),
			x = y = this.setting.border_size,
			text_array = [];

		x += this.setting.text_offsetX;
		y += this.setting.text_offsetY;

		if (this.setting.block_type == 'left'){
			x = size.x + this.setting.block_offset - this.setting.border_size + setting.text_offsetX;
		}

		if (this.setting.block_type == 'down'){
			y = size.y + this.setting.block_offset - this.setting.border_size + this.setting.text_offsetY;
		}

		for(var key in this.textObj){
			var textObj = this.textObj[key];

			this.context.font = textObj.font || this.setting.text_font;
			this.context.fillStyle = textObj.color || this.setting.text_color;
			this.context.textAlign = textObj.textAlign || this.setting.text_align;
			this.context.textBaseline = textObj.textBaseline || this.setting.text_baseline;

			var text = textObj.text.replace('#', textObj.value);

			this.context.fillText(text, x,y);

			if(this.setting.block_type == 'left')
				y += this.setting.text_lineOffset;

			if(this.setting.block_type == 'down')
				x += this.setting.text_lineOffset;

		}
	};

	this.draw_map = function(){
		if(!this.map.data) return;
		var view_radius = this.player.view_radius;
		var view_size = this.setting.view_size;

		var min = view_size - view_radius,
			max = view_size + view_radius + 1;

		var p_x = this.player.x - view_size,
		 	p_y = this.player.y - view_size;

		for (var y = min + p_y; y < max + p_y; y++){
			for(var x = min + p_x; x < max + p_x; x++){					
				var n_x = x//p_x + x;
				var n_y = y//p_y + y;

				var t_x = ((x - p_x) * this.setting.tile_size);
				var t_y = ((y - p_y) * this.setting.tile_size);

				var obj = color = type = null;

				obj = this.get_mapObj('wall');
				color = obj.color;
			 	type = obj.type;

				if(this.map.data[y])
					if(this.map.data[y][x]){ 
						obj = this.get_mapObj(this.map.data[y][x]);
						color = obj.color;
					 	isBonus = obj.bonus;

						if(isBonus){
							this.draw_tile(t_x, t_y, this.get_mapObj('empty').color);
							this.draw_bonus(t_x, t_y, color);
						} else { 
							this.draw_tile(t_x, t_y, color);
						}
					} else 
						this.draw_tile(t_x, t_y, color);
				else 
					this.draw_tile(t_x, t_y, color);
			}
		}

		if(this.setting.view_borderColor && view_size > view_radius){
			for(var y = min - 1; y < max+1; y++)
				for(var x = min - 1; x < max+1; x++)
					if(y == min - 1 || y == max || x == min - 1 || x == max)
						this.draw_tile(
							x*this.setting.tile_size, 
							y*this.setting.tile_size, 
							this.setting.view_borderColor
						)
		}
	};

	this.draw_bonus = function(x, y, color){
		x += this.setting.tile_size/2+ this.setting.border_size;
		y += this.setting.tile_size/2+ this.setting.border_size;

		this.context.fillStyle = color;
		this.context.beginPath();
		this.context.arc(
			x,
			y,
			this.setting.tile_size / 3 - 1,
			0,
			Math.PI * 2
		);
		this.context.fill();
	};

	this.draw = function(type){
		this.draw_border();
		this.draw_block();

		this.draw_map();
		this.draw_player();
	}
// end //

// TODO: поправить
// move //
	this.move = {};
	this.move.timer = {};

	this.move.player = function(key, type){
		if(typeof(key) == 'number') key = self.eKey.fKey(key);
		if(type=='keyup') {
			self.move.timer[key] = null;
		};
		if((self.move.timer['left'] && key=='right') ||
		   (self.move.timer['up'] && key=='down') ||
		   (self.move.timer['down'] && key=='up') ||
		   (self.move.timer['right'] && key=='left')) return;

		var player = self.player;
		var x = player.x,
			y = player.y;

		if(self.eKey.keyPress[key]){
			var down  = self.get_tile(x, y+1).wall,
				up    = self.get_tile(x, y-1).wall,
				right = self.get_tile(x+1, y).wall,
				left  = self.get_tile(x-1, y).wall,
				obj = self.get_tile(x, y),
				name = self.get_nameTile(x, y);

			switch(key){
				case 'up':
					if(!up) player.y--;
					break;
				case 'down':
					if(!down) player.y++;
					break;
				case 'right':
					if(!right) player.x++;
					break;
				case 'left':
					if(!left) player.x--;
					break;
				case 'enter':
					if(obj.script && obj.event_type == 'clkEnter'){
						obj.script.call(this, obj);
						this.events.call(name);
					};
					break;
			};

			name = self.get_nameTile(player.x, player.y);
			obj = self.get_tile(player.x, player.y);

			if(obj.script){
				if(obj.event_type == 'enter' || !obj.event_type){
				  	obj.script.call(self, obj);
					self.events.call(name);
				}
			}

			if(key!='enter'){
				self.move.timer[key] = setTimeout(function(){
					self.move.player.apply(self.move, [key, type])
				}, 100)
			}
		}
	}

	this.move.init = function() {
		self.eKey.add('down', this.player);
		self.eKey.add('up', this.player);
		self.eKey.add('left', this.player);
		self.eKey.add('right', this.player);
		self.eKey.add('enter', this.player);

	};
// end //

// init //
	this.init = function(){
		console.log('init')
		var size = this.get_size();
		this.isStart = true;
		this.canvas.width = size.x;
		this.canvas.height = size.y;

		this.save.mapObj = this.clone(this.mapObj);
		this.save.player = this.clone(this.player);
		this.save.setting = this.clone(this.setting);

		this.eKey.init(window);
		this.move.init();

		this.loop();
		this.modules.init();

		this.map_gen();
		this.pos_generate();
		this.draw();
	};

	this.gameStart = function(){
		this.isStart = true;
		this.modules.reset();
		this.map_gen();
		this.pos_generate();
		this.draw();
	};

	this.gameEnd = function(){
		this.isStart = false;
		// draw.gameMenu
	}
// end //

// loop //
	this.loop = function(){
		if(this.isStart){
			this.draw();
		}

		setTimeout(this.loop.bind(this), 1000/60);
	}
// end //

}
var game = new gameJS(document.getElementsByClassName('canvas')[0]);
game.init();