var game = function(canvas){

// init //
	var self = this;	
	
	self.sizeBlock = 20;
	self.sizeMap = 6;
	self.sizeInfoBlock = 10;
	
	var size = self.sizeBlock * self.sizeMap;
	
	self.canvas = canvas;
	canvas.width = size;
	canvas.heigth = size + self.sizeInfoBlock;
	
	self.ctx = canvas.getContext("2d");
	
	self.player = {
		x: null,
		y: null,
		score: 0
	};
	
	self.map = [];
	
	self.objGame = {
		next: {
			random: 0,
			event: function(){
				
			}
		} 
	};
	
	self.objMap = [
		// {id: name}
	];
// end //

// event //
	window.onkeydown = function(e) {
		var key = e.keyCode;
		var keyMap = {38: "up", 39: "rigth", 40: "down", 37: "left"};
		self.move(keyMap[key]);
	}
// end //

	self.move = function(key){
		var x = self.player.x,
			y = self.player.y;

		switch (key) {
			case "up": y++; break;
			case "rigth": x++; break;
			case "down": y--; break;
			case "left": y++; break;	
		};

		var id = self.map[y][x];

		if((id == 1) || (self.objGame[self.objMap[id]].wall)) return;

		self.objMap.forEach(function(e){
			if (e.pos.toString() == pos.toString()) self.objGame[e.name].event(self);
		});
		
	}
	
}