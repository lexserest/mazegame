log=function(arr) {
	out = '';
	for(y = 0; y < arr.length; y++){
		for (x = 0; x < arr[y].length; x++){
			out += (arr[y][x] || '0') + ' ';
		}
		out+='\n';
	}
	console.log(out);
}

gen=function(x_size, y_size){
	// d = document.getElementsByClassName('canvas')[0].getContext('2d');
	// d.fillStyle = '#000';
	// d.fillRect(0,0,100,100)
	// Объявим массивы для хранения значения множества текущей ячейки, для значения стенки справа и для значения стенки снизу
	 
	var arr = [];
	for(y = 0; y < y_size*2+1; y++){
		arr[y] = [];
		for (x = 0; x < x_size*2+1; x++){
			arr[y][x] = 0;
			if(y==0 || (!(x%2)) ||(x%2 && !(y%2)) || y==y_size*2 || x==x_size*2) 
			arr[y][x] = 1;
		}
	}

	var mn = Array(x_size),
		b = Array(x_size),
		k = Array(x_size), 
		q = 1;


	// Цикл по строкам
	for (y = 0; y < y_size; y++) {
		// Проверка принадлежности ячейки в строке к какому-либо множеству     
		for (x = 0; x < x_size; x++){
			if(0 == y) mn[x] = 0;
			//d.clearRect(13 * x + 3, 13 * y + 3, 10, 10);
			k[x] = 0;
			if(1 == b[x]) b[x] = mn[x] = 0;
			if(0 == mn[x]) mn[x] = q++;
		}

		// Создание случайным образом стенок справа и снизу
		for (x = 0; x < x_size; x++) {
			k[x] = Math.floor(2 * Math.random()); 
			b[x] = Math.floor(2 * Math.random());

			if ((0 == k[x] || y == y_size - 1) //право
			&& x != x_size - 1 
			&& mn[x + 1] != mn[x]) {
				var l = mn[x + 1];
				for (j = 0; j < x_size; j++) if(mn[j] == l) mn[j] = mn[x];
				//d.clearRect(13 * x + 3, 13 * y + 3, 15, 10)
				arr[y*2+1][x*2+2]=0;
			}
			if(y != y_size - 1 && 0 == b[x]) {// низ
				//d.clearRect(13 * x + 3, 13 * y + 3, 10, 15)
				arr[y*2+2][x*2+1]=0;
			}
		}

		 // Проверка на замкнутые области.
		for (x = 0; x < x_size; x++) {
			var p = 0,
				l = 0;
			for (j = 0; j < x_size; j++) 
				if(mn[x] == mn[j] && 0 == b[j]) p++; 
				else l++;
			if(0 == p){
				b[x] = 0;
				arr[y*2+2][x*2+1]=0;
				//d.clearRect(13 * x + 3, 13 * y + 3, 10, 15)
			}
		}
	}


	log(arr);

	return arr;
}

gen(5,5);